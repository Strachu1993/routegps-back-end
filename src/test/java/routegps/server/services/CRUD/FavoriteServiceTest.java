package routegps.server.services.CRUD;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import routegps.server.config.extended.MockExtension;
import routegps.server.dataBase.models.Favorite;
import routegps.server.services.JpaService;
import routegps.server.services.MessageService;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.*;

@ExtendWith({ SpringExtension.class, MockExtension.class })
@SpringBootTest
class FavoriteServiceTest {

    @Mock
    private JpaService jpaService;

    @Mock
    private MessageService messageService;

    private FavoriteService favoriteService;

    @BeforeEach
    void setUp() {
        favoriteService = new FavoriteService(jpaService, messageService);
    }

    @Test
    void shouldReturnFavoritesListWithFourElements(@Qualifier("favoriteListWithFourElementsBean") List<Favorite> testDataForFavoriteList) throws Exception {
        // given
        given(jpaService.getFavorites()).willReturn(testDataForFavoriteList);

        // when
        List<Favorite> favoriteList = jpaService.getFavorites();

        // then
        then(jpaService).should(times(1)).getFavorites();

        assertAll(
                () -> assertNotNull(favoriteList),
                () -> assertEquals(favoriteList.size(), 4)
        );
    }

    @Test
    void shouldReturnHttpEntityWhenFavoriteRepoCallSave() throws Exception {
        // given
        final String httpBody = "response.text.add";
        final HttpStatus httpStatus = HttpStatus.CREATED;
        final Favorite favorite = Favorite.builder().name("Hotel").build();
        final HttpEntity responseEntity = ResponseEntity.status(httpStatus)
                                        .contentType(MediaType.TEXT_PLAIN)
                                        .body(httpBody);

        doNothing().when(jpaService).addFavorite(favorite);
        given(messageService.getHttpEntityBody(httpBody, httpStatus)).willReturn((ResponseEntity<String>) responseEntity);

        // when
        HttpEntity httpEntity = favoriteService.addFavorite(favorite);

        // then
        then(jpaService).should(timeout(1)).addFavorite(favorite);
        then(messageService).should(timeout(1)).getHttpEntityBody(httpBody, httpStatus);

        assertAll(
                () -> assertNotNull(httpEntity),
                () -> assertEquals(httpEntity.getBody(), httpBody)
        );
    }

    @Test
    void editFavoriteName() {
    }

    @Test
    void removeFavoriteByName() {
    }

}