package routegps.server.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import routegps.server.config.extended.MockExtension;
import routegps.server.dataBase.models.Favorite;
import routegps.server.dataBase.models.GpsData;
import routegps.server.dataBase.models.Route;
import routegps.server.dataBase.models.RouteCatalog;
import routegps.server.dataBase.repositories.FavoriteRepository;
import routegps.server.dataBase.repositories.GpsDataRepository;
import routegps.server.dataBase.repositories.RouteCatalogRepository;
import routegps.server.dataBase.repositories.RouteRepository;
import routegps.server.dto.DateDto;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.*;

@ExtendWith({ SpringExtension.class, MockExtension.class })
@SpringBootTest
public class JpaServiceTest {

    @Mock
    private RouteRepository routeRepository;

    @Mock
    private RouteCatalogRepository routeCatalogRepository;

    @Mock
    private FavoriteRepository favoriteRepository;

    @Mock
    private GpsDataRepository gpsDataRepository;

    private JpaService jpaService;

    @BeforeEach
    public void setUp(){
        jpaService = new JpaService(favoriteRepository, routeRepository, routeCatalogRepository, gpsDataRepository);
    }

    @Test
    public void shouldReturnRouteListWithFourElements(@Qualifier("routeListWithFourElementsBean") List<Route> testDataForRouteList) throws Exception {
        // given
        given(routeRepository.findAll()).willReturn(testDataForRouteList);

        // when
        List<Route> routeList = jpaService.getRoutes();

        // then
        then(routeRepository).should(times(1)).findAll();
        assertAll(
                () -> assertNotNull(routeList),
                () -> assertEquals(routeList.size(), 4)
        );
    }

    @Test
    void shouldReturnFavoritesListWithFourElements(@Qualifier("favoriteListWithFourElementsBean") List<Favorite> testDataForFavoriteList) throws Exception {
        // given
        given(favoriteRepository.findAll()).willReturn(testDataForFavoriteList);

        // when
        List<Favorite> favoriteList = jpaService.getFavorites();

        // then
        then(favoriteRepository).should(times(1)).findAll();
        assertAll(
                () -> assertNotNull(favoriteList),
                () -> assertEquals(favoriteList.size(), 4)
        );
    }

    @Test
    void shouldReturnRouteCatalogListWithFourElements(@Qualifier("routeCatalogListWithFourElementsBean") List<RouteCatalog> testDataForRouteCatalogList) throws Exception {
        // given
        given(routeCatalogRepository.findAll()).willReturn(testDataForRouteCatalogList);

        // when
        List<RouteCatalog> routeCatalogList = jpaService.getRouteCatalogs();

        // then
        then(routeCatalogRepository).should(times(1)).findAll();
        assertAll(
                () -> assertNotNull(routeCatalogList),
                () -> assertEquals(routeCatalogList.size(), 4)
        );
    }

    @Test
    void shouldReturnGpsDataListWithFourElements(@Qualifier("gpsDataListWithFourElementBean") List<GpsData> testDataForGpsDataList) throws Exception {
        // given
        final DateDto date = DateDto.GET_FULL_DATE();
        final String name = "routeCatalogName";
        final Route route = Route.builder().gpsDataList(testDataForGpsDataList).build();

        given(routeRepository.findByDateBeginAndRouteCatalogName(date, name)).willReturn(route);

        // when
        List<GpsData> gpsDataList = jpaService.getGpsDataByDateBeginAndRouteCatalogName(date, name);

        // then
        then(routeRepository).should(times(1)).findByDateBeginAndRouteCatalogName(date, name);

        assertAll(
                () -> assertNotNull(gpsDataList),
                () -> assertEquals(gpsDataList.size(), 4)
        );
    }

    @Test
    void shouldReturnNullPointerExceptionWhenGpsDataListIsEmpty() throws NullPointerException {
        // given
        final DateDto testDate = DateDto.GET_FULL_DATE();
        final String noExistsName = "no Exists Name";

        given(routeRepository.findByDateBeginAndRouteCatalogName(testDate, noExistsName)).willReturn(new Route());

        // when
        List<GpsData> gpsDataListNoExistsName = jpaService.getGpsDataByDateBeginAndRouteCatalogName(testDate, noExistsName);

        // then
        then(routeRepository).should(times(1)).findByDateBeginAndRouteCatalogName(testDate, noExistsName);

        assertThrows(NullPointerException.class, () -> {
            if(gpsDataListNoExistsName.isEmpty())
                throw new NullPointerException();
        });
    }

    @Test
    void shouldDoNothingWhenFavoriteRepoCallSave(){
        // given
        final Favorite favoriteData = Favorite.builder().name("restaurant").build();
        given(favoriteRepository.save(favoriteData)).willReturn(favoriteData);

        // when
        jpaService.addFavorite(favoriteData);

        // then
        then(favoriteRepository).should(times(1)).save(favoriteData);
    }

    @Test
    void shouldDoNothingWhenFavoriteRepoEditName(){
        // given
        final String name = "restaurant";
        final String nameResult = "hotel";
        final Favorite favoriteData = Favorite.builder().name(name).build();
        final Favorite favoriteResult = Favorite.builder().name(nameResult).build();

        given(favoriteRepository.findByName(name)).willReturn(favoriteData);
        given(favoriteRepository.save(favoriteResult)).willReturn(favoriteResult);

        // when
        jpaService.editFavoriteName(name, nameResult);

        // then
        then(favoriteRepository).should(times(1)).findByName(name);
        then(favoriteRepository).should(times(1)).save(favoriteResult);
    }

    @Test
    void shouldDoNothingWhenFavoriteRepoWillDeleteRow(){
        // given
        final String name = "Favorite place";
        final Favorite favorite = Favorite.builder().name(name).build();

        given(favoriteRepository.findByName(name)).willReturn(favorite);
        doNothing().when(favoriteRepository).delete(favorite);

        // when
        jpaService.removeFavoriteByName(name);

        // then
        then(favoriteRepository).should(times(1)).findByName(name);
        then(favoriteRepository).should(times(1)).delete(favorite);
    }

    @Test
    void shouldDoNothingWhenRouteCatalogRepoWillAddRow(){
        // given
        final String name = "Route name";
        final RouteCatalog routeCatalog = RouteCatalog.builder().name(name).build();

        given(routeCatalogRepository.save(routeCatalog)).willReturn(routeCatalog);

        // when
        jpaService.addRouteCatalog(routeCatalog);

        // then
        then(routeCatalogRepository).should(times(1)).save(routeCatalog);
    }

    @Test
    void shouldDoNothingWhenRouteCatalogRepoWillEditRow(){
        // given
        final String name = "Route name";
        final String nameResult = "Edit Route name";
        final RouteCatalog routeCatalog = RouteCatalog.builder().name(name).build();
        final RouteCatalog routeCatalogResult = RouteCatalog.builder().name(nameResult).build();

        given(routeCatalogRepository.findByName(name)).willReturn(routeCatalog);
        given(routeCatalogRepository.save(routeCatalogResult)).willReturn(routeCatalogResult);

        // when
        jpaService.editRouteCatalog(name, routeCatalogResult);

        // then
        then(routeCatalogRepository).should(times(1)).findByName(name);
        then(routeCatalogRepository).should(times(1)).save(routeCatalogResult);
    }

    @Test
    void shouldDoNothingWhenRouteRepoWillSaveRouteAndGpsDataList(@Qualifier("gpsDataListWithFourElementBean") List<GpsData> testDataForGpsDataList){
        // given
        final String routeCatalogName = "Hotel";
        final RouteCatalog routeCatalog = RouteCatalog.builder().name(routeCatalogName).build();
        final Route routeResult = Route.builder().gpsDataList(testDataForGpsDataList).build();

        given(routeCatalogRepository.findByName(routeCatalogName)).willReturn(routeCatalog);
        given(gpsDataRepository.saveAll(testDataForGpsDataList)).willReturn(testDataForGpsDataList);
        given(routeRepository.save(routeResult)).willReturn(any());

        // when
        jpaService.addRouteAndGpsData(routeCatalogName, testDataForGpsDataList);

        // then
        then(routeCatalogRepository).should(times(1)).findByName(routeCatalogName);
        then(gpsDataRepository).should(times(1)).saveAll(testDataForGpsDataList);
        then(routeRepository).should(times(1)).save(any());
    }

    @Test
    void shouldDoNothingWhenRouteCatalogRepoWillDeleteRowByName(){
        // given
        final String routeCatalogName = "Hotel";
        final RouteCatalog routeCatalog = RouteCatalog.builder().name(routeCatalogName).build();

        given(routeCatalogRepository.findByName(routeCatalogName)).willReturn(routeCatalog);
        doNothing().when(routeCatalogRepository).delete(routeCatalog);

        // when
        jpaService.removeRouteCatalogByName(routeCatalogName);

        // then
        then(routeCatalogRepository).should(times(1)).findByName(routeCatalogName);
        then(routeCatalogRepository).should(times(1)).delete(routeCatalog);
    }

}