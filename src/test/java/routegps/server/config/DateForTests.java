package routegps.server.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import routegps.server.dataBase.models.Favorite;
import routegps.server.dataBase.models.GpsData;
import routegps.server.dataBase.models.Route;
import routegps.server.dataBase.models.RouteCatalog;
import routegps.server.dto.DateDto;

import java.awt.geom.Point2D;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Configuration
public class DateForTests {

    private int howMany = 4;

    @Bean
    List<Route> routeListWithFourElementsBean(){
        List<Route> routeList = new ArrayList<>();

        for(int i=1 ; i<howMany + 1 ; i++){
            routeList.add(routeBuild(i));
        }

        return routeList;
    }

    @Bean
    List<Favorite> favoriteListWithFourElementsBean(){
        List<Favorite> favoriteList = new ArrayList<>();

        for(int i=1 ; i<howMany + 1 ; i++){
            favoriteList.add(favoriteBuild(i));
        }

        return favoriteList;
    }

    @Bean
    List<RouteCatalog> routeCatalogListWithFourElementsBean(){
        List<RouteCatalog> routeCatalogList = new ArrayList<>();

        for(int i=1 ; i<howMany + 1 ; i++){
            routeCatalogList.add(routeCatalogBuild(i));
        }

        return routeCatalogList;
    }

    @Bean
    List<GpsData> gpsDataListWithFourElementBean(){
        List<GpsData> gpsDataList = new ArrayList<>();

        for(int i=1 ; i<howMany + 1 ; i++){
            gpsDataList.add(gpsDataBuild(i));
        }

        return gpsDataList;
    }

    private GpsData gpsDataBuild(int i) {
        return GpsData.builder()
                .points(new Point2D.Double(33.33 + i, 33.33 + i))
                .speed((short) (5 + i))
                .date(getTestDate(i))
                .build();
    }

    private Route routeBuild(int i) {
        return Route.builder()
                .dateBegin(getTestDate(i))
                .dateEnd(getTestDate(i))
                .build();
    }

    private Favorite favoriteBuild(int i) {
        return Favorite.builder()
                .name("Favorite name " + i)
                .points(new Point2D.Double(44.44 + i, 22.22 + i))
                .build();
    }

    private RouteCatalog routeCatalogBuild(int i) {
        return RouteCatalog.builder()
                .name("Route Catalog name " + i)
                .count((short) i)
                .note("Example note " + i)
                .build();
    }

    private DateDto getTestDate(int i){
        LocalDate date = LocalDate.of(2017, 04, 17)
                .plusDays(i)
                .plusMonths(i)
                .plusDays(i);

        LocalTime time = LocalTime.of(15, 33, 55)
                .plusHours(i)
                .plusMinutes(i)
                .plusSeconds(i);

        return new DateDto(date, time);
    }

}
