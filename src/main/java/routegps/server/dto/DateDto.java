package routegps.server.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;

@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class DateDto implements Serializable {

    @JsonFormat(pattern = "dd-MM-yyyy")
    private LocalDate date;

    @JsonFormat(pattern = "HH:mm:ss.SSS")
    private LocalTime time;

    public DateDto(String date, String time){
        changeTextToDate(date);
        changeTextToTime(time);
    }

    public static final DateDto GET_FULL_DATE(){
        return new DateDto(LocalDate.now(), LocalTime.now());
    }

    private void changeTextToDate(String date) {
        this.date = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
    }

    private void changeTextToTime(String time) {
        this.time = LocalTime.parse(time, DateTimeFormatter.ofPattern("HH:mm:ss.SSS"));
    }

    public static class DateDtoComparator implements Comparator<DateDto> {

        @Override
        public int compare(DateDto o1, DateDto o2) {
            if(null == o1 || null == o2) {
                throw new NullPointerException();
            }

            int compareDate = o1.getDate().compareTo(o2.getDate());

            if(compareDate == 0){
                return o1.getTime().compareTo(o2.getTime());
            }

            return compareDate;
        }

    }

}
