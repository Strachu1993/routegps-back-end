package routegps.server.dataBase.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import routegps.server.dataBase.models.Favorite;

@Repository
public interface FavoriteRepository extends JpaRepository<Favorite, Long> {

    Favorite findByName(String name);

}