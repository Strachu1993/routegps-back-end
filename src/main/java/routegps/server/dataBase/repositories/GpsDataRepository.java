package routegps.server.dataBase.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import routegps.server.dataBase.models.GpsData;

public interface GpsDataRepository extends JpaRepository<GpsData, Long> {
}
