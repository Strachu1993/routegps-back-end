package routegps.server.dataBase.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import routegps.server.dataBase.models.GpsData;
import routegps.server.dataBase.models.Route;
import routegps.server.dto.DateDto;

import java.util.List;

public interface RouteRepository extends JpaRepository<Route, Long> {

    Route findByDateBeginAndRouteCatalogName(DateDto dateBegin, String name);

    List<Route> findByRouteCatalogName(String name);

}
