package routegps.server.dataBase.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import routegps.server.dataBase.models.RouteCatalog;

public interface RouteCatalogRepository extends JpaRepository<RouteCatalog, Long> {

    RouteCatalog findByName(String name);

}
