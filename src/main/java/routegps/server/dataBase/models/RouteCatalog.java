package routegps.server.dataBase.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Builder
@Setter
@Getter
@ToString(exclude = {"routeList"})
@EqualsAndHashCode(exclude = {"routeList"})
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Route_Catalog")
public class RouteCatalog {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Setter(AccessLevel.PROTECTED)
    @JsonIgnore
    @Column(name = "id")
    private long id;

    @Size(min = 1)
    @Column(name = "name", unique = true)
    private String name;

    @Column(name = "count")
    private short count = 0;

    @Size(max = 300)
    @Column(name = "note")
    private String note;

    @JsonIgnore
    @OneToMany(mappedBy = "routeCatalog", cascade = CascadeType.REMOVE)
    private List<Route> routeList = new ArrayList<>();

}
