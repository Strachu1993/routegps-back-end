package routegps.server.dataBase.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.awt.geom.Point2D;

@Builder
@Setter
@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Favorite")
public class Favorite {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Setter(AccessLevel.PROTECTED)
    @JsonIgnore
    @Column(name = "id")
    private long id;

    @Column(name = "points", nullable = false)
    private Point2D.Double points;

    @NotEmpty
    @Size(min = 1)
    @Column(name = "name", unique = true)
    private String name;

}
