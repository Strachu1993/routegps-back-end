package routegps.server.dataBase.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import routegps.server.dto.DateDto;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Builder
@Setter
@Getter
@ToString(exclude = {"gpsDataList"})
@EqualsAndHashCode(exclude = {"gpsDataList"})
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Route")
public class Route {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Setter(AccessLevel.PROTECTED)
    @JsonIgnore
    @Column(name = "id")
    private long id;


    @Column(name = "date_begin", unique = true)
    private DateDto dateBegin;

    @Column(name = "date_end")
    private DateDto dateEnd;

    @JsonIgnore
    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "routeCatalog")
    private RouteCatalog routeCatalog;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.REMOVE)
    private List<GpsData> gpsDataList = new ArrayList<>();

}
