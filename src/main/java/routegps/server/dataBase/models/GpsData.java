package routegps.server.dataBase.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import routegps.server.dto.DateDto;
import javax.persistence.*;
import java.awt.geom.Point2D;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
@Table(name = "Gps_Data")
public class GpsData {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Setter(AccessLevel.PROTECTED)
    @JsonIgnore
    @Column(name = "id")
    private long id;

    @Column(name = "points", nullable = false)
    private Point2D.Double points;

    @Column(name = "speed")
    private Short speed;

    @Column(name = "date", nullable = false)
    private DateDto date;

    public GpsData(String data, String time, Short speed, Point2D.Double points){
        this.points = points;
        this.speed = speed;
        this.date = new DateDto(data, time);
    }

    public static final GpsData NEW_GPS_DATA(Point2D.Double points, short speed){
        GpsData gpsData = new GpsData();
        gpsData.setPoints(points);
        gpsData.setSpeed(speed);
        gpsData.setDate(DateDto.GET_FULL_DATE());
        return gpsData;
    }

}
