package routegps.server.exceptions;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import routegps.server.services.MessageService;

import javax.validation.ConstraintViolationException;

@AllArgsConstructor(onConstructor = @__({@Autowired}))
@ControllerAdvice
public class ExceptionsControllersHandlers extends ResponseEntityExceptionHandler {

    private final MessageService messageService;

    @ExceptionHandler({ org.hibernate.exception.ConstraintViolationException.class })
    public ResponseEntity<Object> handleConstraintViolationExceptionException(RuntimeException ex, WebRequest request){
        return handleExceptionInternal(ex, messageService.getMessage("exception.constraint.violation"), new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    @ExceptionHandler({ javax.persistence.RollbackException.class})
    public ResponseEntity<Object> handleRollbackException(RuntimeException ex, WebRequest request){
        return handleExceptionInternal(ex, messageService.getMessage("exception.rollback"), new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    @ExceptionHandler({ java.lang.NullPointerException.class, java.lang.IllegalArgumentException.class  })
    public ResponseEntity<Object> handleNullException(RuntimeException ex, WebRequest request){
        return handleExceptionInternal(ex, messageService.getMessage("exception.null"), new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

}
