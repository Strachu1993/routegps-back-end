package routegps.server.controllers;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import routegps.server.dataBase.models.Favorite;
import routegps.server.services.CRUD.FavoriteService;

import java.util.List;

@AllArgsConstructor(onConstructor = @__({@Autowired}))
@RequestMapping(value = "/favorite", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
public class FavoriteController {

    private final FavoriteService favoriteService;

    @ResponseBody
    @GetMapping(value = "/get")
    public List<Favorite> getFavorites(){
        return favoriteService.getFavorites();
    }

    @PostMapping(value = "/add")
    public void addFavorite(@RequestBody Favorite newFavorite){
        favoriteService.addFavorite(newFavorite);
    }

    @PutMapping(value = "/editName")
    public void editFavoriteName(@RequestParam String oldName, @RequestParam String newName){
        favoriteService.editFavoriteName(oldName, newName);
    }

    @DeleteMapping(value = "/remove")
    public void removeFavoriteByName(@RequestParam String name){
        favoriteService.removeFavoriteByName(name);
    }

}
