package routegps.server.controllers;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import routegps.server.dataBase.models.RouteCatalog;
import routegps.server.services.CRUD.RouteCatalogService;

import java.util.List;

@AllArgsConstructor(onConstructor = @__({@Autowired}))
@RequestMapping(value = "/route/catalog", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
public class RouteCatalogController {

    private final RouteCatalogService routeCatalogService;

    @ResponseBody
    @GetMapping(value = "/get")
    public List<RouteCatalog> getRouteCatalogs(){
        return routeCatalogService.getRouteCatalogs();
    }

    @PostMapping(value = "/add")
    public void addRouteCatalog(@RequestBody RouteCatalog newRouteCatalog){
        routeCatalogService.addRouteCatalog(newRouteCatalog);
    }

    @PutMapping(value = "/edit")
    public void editRouteCatalog(@RequestParam String name, @RequestBody RouteCatalog newRouteCatalog){
        routeCatalogService.editRouteCatalog(name, newRouteCatalog);
    }

    @DeleteMapping(value = "/remove")
    public void removeRouteCatalogByName(@RequestParam String name){
        routeCatalogService.removeRouteCatalogByName(name);
    }

}
