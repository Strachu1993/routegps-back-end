package routegps.server.controllers;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import routegps.server.dataBase.models.GpsData;
import routegps.server.dataBase.models.Route;
import routegps.server.dto.DateDto;
import routegps.server.services.CRUD.RouteService;

import java.awt.geom.Point2D;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@AllArgsConstructor(onConstructor = @__({@Autowired}))
@RequestMapping(value = "/route", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
public class RouteController {

    private final RouteService routeService;

    @ResponseBody
    @GetMapping(value = "/get")
    public List<Route> getRoutes(@RequestParam String routeCatalogName){
        return routeService.getRoutes(routeCatalogName);
    }

    @ResponseBody
    @PostMapping(value = "/getGpsData")
    public List<GpsData> getGpsDataByDateBegin(@RequestParam String routeCatalogName, @RequestBody DateDto dateBegin){
        return routeService.getGpsDataByDateBeginAndRouteCatalogName(dateBegin, routeCatalogName);
    }

    @PostMapping(value = "/addRouteAndGpsData")
    public void addRouteAndGpsData(@RequestParam String routeCatalogName, @RequestBody List<GpsData> gpsDataList){
        routeService.addRouteAndGpsData(routeCatalogName, gpsDataList);
    }

}
