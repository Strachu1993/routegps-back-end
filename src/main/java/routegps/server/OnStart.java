package routegps.server;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import routegps.server.services.InitDataBaseService;
import routegps.server.services.JpaService;

import javax.annotation.PostConstruct;

@AllArgsConstructor(onConstructor = @__({@Autowired}))
@Component
public class OnStart {

    private InitDataBaseService initDataBaseService;

    @PostConstruct
    public void onStart(){
        initDataBaseService.initDataBase();
        initDataBaseService.logDataFromDataBase();
    }

}
