package routegps.server.services;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import routegps.server.dataBase.models.Favorite;
import routegps.server.dataBase.models.GpsData;
import routegps.server.dataBase.models.Route;
import routegps.server.dataBase.models.RouteCatalog;
import routegps.server.dataBase.repositories.FavoriteRepository;
import routegps.server.dataBase.repositories.GpsDataRepository;
import routegps.server.dataBase.repositories.RouteCatalogRepository;
import routegps.server.dataBase.repositories.RouteRepository;
import routegps.server.dto.DateDto;

import java.util.List;

@AllArgsConstructor(onConstructor = @__({@Autowired}))
@Service
public class JpaService {

    private final FavoriteRepository favoriteRepository;
    private final RouteRepository routeRepository;
    private final RouteCatalogRepository routeCatalogRepository;
    private final GpsDataRepository gpsDataRepository;

    public List<RouteCatalog> getRouteCatalogs(){
        return routeCatalogRepository.findAll();
    }

    public List<Route> getRoutes(String routeCatalogName){
        return routeRepository.findByRouteCatalogName(routeCatalogName);
    }

    public List<Favorite> getFavorites() {
        return favoriteRepository.findAll();
    }

    public List<GpsData> getGpsDataByDateBeginAndRouteCatalogName(DateDto dateBegin, String routeCatalogName){
        return routeRepository.findByDateBeginAndRouteCatalogName(dateBegin, routeCatalogName).getGpsDataList();
    }

    public void addFavorite(Favorite newFavorite) {
        favoriteRepository.save(newFavorite);
    }

    public void editFavoriteName(String name, String newName){
        Favorite favorite = favoriteRepository.findByName(name);
        isNullThenThrowNullException(favorite);
        changeFavoriteNameAndSave(newName, favorite);
    }

    public void removeFavoriteByName(String name){
        favoriteRepository.delete(favoriteRepository.findByName(name));
    }

    public void addRouteCatalog(RouteCatalog newRouteCatalog){
        routeCatalogRepository.save(RouteCatalog.builder()
                .name(newRouteCatalog.getName())
                .note(newRouteCatalog.getNote())
                .build()
        );
    }

    public void editRouteCatalog(String name, RouteCatalog newRouteCatalog){
        RouteCatalog routeCatalog = routeCatalogRepository.findByName(name);
        isNullThenThrowNullException(routeCatalog);
        changeRouteCatalogAndSave(newRouteCatalog, routeCatalog);
    }

    public void addRouteAndGpsData(String routeCatalogName, List<GpsData> gpsDataList){
        RouteCatalog routeCatalog = routeCatalogRepository.findByName(routeCatalogName);
        createRouteCatalogIfNotExists(routeCatalogName, routeCatalog);
        isNullThenThrowNullException(gpsDataList);
        routeCatalog = routeCatalogRepository.findByName(routeCatalogName);
        saveGpsDataList(gpsDataList);
        saveRoute(gpsDataList, routeCatalog);
    }

    public void removeRouteCatalogByName(String name){
        RouteCatalog routeCatalog = routeCatalogRepository.findByName(name);
        isNullThenThrowNullException(routeCatalog);
        routeCatalogRepository.delete(routeCatalog);
    }

    private void createRouteCatalogIfNotExists(String routeCatalogName, RouteCatalog routeCatalog) {
        if(null == routeCatalog){
            addRouteCatalog(RouteCatalog.builder()
                    .name(routeCatalogName)
                    .build());
        }
    }

    private void saveRoute(List<GpsData> gpsDataList, RouteCatalog routeCatalog) {
        routeRepository.save(Route.builder()
                .dateBegin(gpsDataList.stream()
                        .map(GpsData::getDate)
                        .min((gps1, gps2) -> new DateDto.DateDtoComparator().compare(gps1, gps2))
                        .get()
                )
                .dateEnd(gpsDataList.stream()
                        .map(GpsData::getDate)
                        .max((gps1, gps2) -> new DateDto.DateDtoComparator().compare(gps1, gps2))
                        .get()
                )
                .gpsDataList(gpsDataList)
                .routeCatalog(routeCatalog)
                .build()
        );
    }

    private void changeFavoriteNameAndSave(String newName, Favorite favorite) {
        favorite.setName(newName);
        favoriteRepository.save(favorite);
    }

    private void changeRouteCatalogAndSave(RouteCatalog newRouteCatalog, RouteCatalog routeCatalog) {
        routeCatalog.setName(newRouteCatalog.getName());
        routeCatalog.setNote(newRouteCatalog.getNote());

        routeCatalogRepository.save(routeCatalog);
    }

    private void saveGpsDataList(List<GpsData> gpsDataList) {
        gpsDataRepository.saveAll(gpsDataList);
    }

    private void isNullThenThrowNullException(Object...objects) {
        for(Object o : objects) {
            if (null == o)
                throw new NullPointerException();
        }
    }

}
