package routegps.server.services.CRUD;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import routegps.server.dataBase.models.Favorite;
import routegps.server.services.JpaService;
import routegps.server.services.MessageService;

import java.util.List;

@AllArgsConstructor(onConstructor = @__({@Autowired}))
@Service
public class FavoriteService {

    private final JpaService jpaService;

    public List<Favorite> getFavorites() {
        return jpaService.getFavorites();
    }

    public void addFavorite(Favorite newFavorite){
        jpaService.addFavorite(newFavorite);
    }

    public void editFavoriteName(String oldName, String newName){
        jpaService.editFavoriteName(oldName, newName);
    }

    public void removeFavoriteByName(String name){
        jpaService.removeFavoriteByName(name);
    }

}
