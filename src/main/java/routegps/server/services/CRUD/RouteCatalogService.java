package routegps.server.services.CRUD;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import routegps.server.dataBase.models.RouteCatalog;
import routegps.server.services.JpaService;
import routegps.server.services.MessageService;

import java.util.List;

@AllArgsConstructor(onConstructor = @__({@Autowired}))
@Service
public class RouteCatalogService {

    private final JpaService jpaService;

    public List<RouteCatalog> getRouteCatalogs(){
        return jpaService.getRouteCatalogs();
    }

    public void addRouteCatalog(RouteCatalog newRouteCatalog){
        jpaService.addRouteCatalog(newRouteCatalog);
    }

    public void editRouteCatalog(String name, RouteCatalog newRouteCatalog){
        jpaService.editRouteCatalog(name, newRouteCatalog);
    }

    public void removeRouteCatalogByName(String name){
        jpaService.removeRouteCatalogByName(name);
    }

}
