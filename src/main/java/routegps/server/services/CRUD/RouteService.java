package routegps.server.services.CRUD;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import routegps.server.dataBase.models.GpsData;
import routegps.server.dataBase.models.Route;
import routegps.server.dto.DateDto;
import routegps.server.services.JpaService;
import routegps.server.services.MessageService;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@AllArgsConstructor(onConstructor = @__({@Autowired}))
@Service
public class RouteService {

    private final JpaService jpaService;

    public List<Route> getRoutes(String routeCatalogName){
        return jpaService.getRoutes(routeCatalogName);
    }

    public List<GpsData> getGpsDataByDateBeginAndRouteCatalogName(DateDto dateBegin, String routeCatalogName){
        return jpaService.getGpsDataByDateBeginAndRouteCatalogName(dateBegin, routeCatalogName);
    }

    public void addRouteAndGpsData(String routeCatalogName, List<GpsData> gpsDataList){
        jpaService.addRouteAndGpsData(routeCatalogName, gpsDataList);
    }

}
