package routegps.server.services;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import routegps.server.dataBase.models.Favorite;
import routegps.server.dataBase.models.GpsData;
import routegps.server.dataBase.models.Route;
import routegps.server.dataBase.models.RouteCatalog;
import routegps.server.dataBase.repositories.FavoriteRepository;
import routegps.server.dataBase.repositories.GpsDataRepository;
import routegps.server.dataBase.repositories.RouteCatalogRepository;
import routegps.server.dataBase.repositories.RouteRepository;
import routegps.server.dto.DateDto;
import routegps.server.logger.MyLogger;
import java.awt.geom.Point2D;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor(onConstructor = @__({@Autowired}))
@Service
public class InitDataBaseService {

    private FavoriteRepository favoriteRepository;
    private GpsDataRepository gpsDataRepository;
    private RouteRepository routeRepository;
    private RouteCatalogRepository routeCatalogRepository;
    private MyLogger myLogger;

    @Transactional
    public void logDataFromDataBase() {

        myLogger.space();
        myLogger.space();

        myLogger.text("Favorite: " + favoriteRepository.count(), 1);
        favoriteRepository.findAll().forEach((array) -> myLogger.text(array, 2));

        myLogger.space();

        myLogger.text("Gps Data: " + gpsDataRepository.count(), 1);
        gpsDataRepository.findAll().forEach(array -> myLogger.text(array, 2));

        myLogger.space();

        myLogger.text("Route catalog: " + routeCatalogRepository.count(), 1);
        routeCatalogRepository.findAll().forEach(array -> {
            myLogger.text(array + " - Routes: " + array.getRouteList().size(), 2);
            array.getRouteList().forEach(routes -> myLogger.text(routes, 3));
        });

        myLogger.space();

        myLogger.text("Route: " + routeRepository.count(), 1);
        routeRepository.findAll().forEach(array -> {
            myLogger.text(array + " - GpsData: " + array.getGpsDataList().size(), 2);
            array.getGpsDataList().forEach(gpsData -> myLogger.text(gpsData, 3));
        });

        myLogger.space();
        myLogger.space();
    }

    @Transactional
    public void initDataBase(){
        initFavoriteTable();
        initGpsDataTable();
        initRouteCatalogTable();
        initRouteTable();
    }

    private void initRouteTable() {
        routeRepository.save(Route.builder()
                .routeCatalog(routeCatalogRepository.findByName("Droga do sklepu"))
                .dateBegin(DateDto.GET_FULL_DATE())
                .dateEnd(DateDto.GET_FULL_DATE())
                .build()
        );

        waitMiliseconds(200);

        routeRepository.save(Route.builder()
                .routeCatalog(routeCatalogRepository.findByName("Droga do sklepu"))
                .dateBegin(DateDto.GET_FULL_DATE())
                .dateEnd(DateDto.GET_FULL_DATE())
                .build()
        );

        waitMiliseconds(200);

        routeRepository.save(Route.builder()
                .routeCatalog(routeCatalogRepository.findByName("Droga na basen"))
                .dateBegin(DateDto.GET_FULL_DATE())
                .dateEnd(DateDto.GET_FULL_DATE())
                .build()
        );

        waitMiliseconds(200);

        routeRepository.save(Route.builder()
                .routeCatalog(routeCatalogRepository.findByName("Droga na basen"))
                .dateBegin(DateDto.GET_FULL_DATE())
                .dateEnd(DateDto.GET_FULL_DATE())
                .build()
        );

        setGpsDataForRoute();
    }

    private void setGpsDataForRoute() {
        List<GpsData> gpsDataList = gpsDataRepository.findAll();

        Route routeOne = routeRepository.findAll().get(0);
        Route routeTwo = routeRepository.findAll().get(1);

        routeOne.setGpsDataList(gpsDataList.stream()
            .filter(gpsData -> gpsData.getId() % 2 == 0)
            .collect(Collectors.toList())
        );

        routeTwo.setGpsDataList(gpsDataList.stream()
            .filter(gpsData -> gpsData.getId() % 2 != 0)
            .collect(Collectors.toList())
        );

        routeRepository.save(routeOne);
        routeRepository.save(routeTwo);
    }

    private void initRouteCatalogTable() {
        routeCatalogRepository.save(RouteCatalog.builder()
                .name("Droga do sklepu")
                .note("Fajna droga")
                .build()
        );

        routeCatalogRepository.save(RouteCatalog.builder()
                .name("Droga na basen")
                .note("Jeszcze lepsza droga do miejsca gdzie jest coś fajnego")
                .build()
        );
    }

    private void initGpsDataTable() {
        double start = 0.000100;
        double next = start;
        for(int i=0 ; i<8 ; i++){
            next += start +start;
            gpsDataRepository.save(GpsData.NEW_GPS_DATA(new Point2D.Double(15.4500064 +next, 51.5560977), (short)2));
        }
    }

    private void initFavoriteTable() {
        favoriteRepository.save(Favorite.builder()
                .points(new Point2D.Double(15.4541478, 51.5583252))
                .name("Dom")
                .build()
        );

        favoriteRepository.save(Favorite.builder()
                .points(new Point2D.Double(15.4501303, 51.5565064))
                .name("Dobra kuchnia")
                .build()
        );

        favoriteRepository.save(Favorite.builder()
                .points(new Point2D.Double(15.4500064, 51.5560977))
                .name("Ledi")
                .build()
        );

        favoriteRepository.save(Favorite.builder()
                .points(new Point2D.Double(-0.12755, 51.507222))
                .name("Londyn")
                .build()
        );

        favoriteRepository.save(Favorite.builder()
                .points(new Point2D.Double(15.5034754, 51.9382711))
                .name("Mafia Sobieskiego")
                .build()
        );

        favoriteRepository.save(Favorite.builder()
                .points(new Point2D.Double(15.5476517, 51.9378382))
                .name("Droga_1")
                .build()
        );

        favoriteRepository.save(Favorite.builder()
                .points(new Point2D.Double(15.4540763, 51.558312))
                .name("Droga_2")
                .build()
        );

        favoriteRepository.save(Favorite.builder()
                .points(new Point2D.Double(15.5488024, 51.9374225))
                .name("Droga_3")
                .build()
        );
    }

    private void waitMiliseconds(int delay) {
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
