package routegps.server.services;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.Locale;

@AllArgsConstructor(onConstructor = @__({@Autowired}))
@Service
public class MessageService {

    private final MessageSource messageSource;

    public String getMessage(String key){
        return messageSource.getMessage(key, null, Locale.getDefault());
    }

    public ResponseEntity<String> getHttpEntityBody(String message, HttpStatus httpStatus) {
        return ResponseEntity.status(httpStatus)
                .contentType(MediaType.TEXT_PLAIN)
                .body(getMessage(message));
    }

}
