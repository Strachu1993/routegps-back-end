# App RouteGPS #

## libraries/technologies ##
	- Java 1.8
	- Spring Boot
	- Spring Data Jpa
	- Data Base - H2 (in memory)
    - Gradle
    - JUnit 5
	- Mockito 2
	- Lombok

### How do I get set up? ###

	1) Intellij Idea
		a) Lombok
			- ctrl+alt+s -> Plugins -> search lombok -> install 'Lombok-Plugin'
			- ctrl+alt+s -> Build. Execution, Deployment -> Compiler -> Annotation Processors -> select 'Enable annotation processing'
			